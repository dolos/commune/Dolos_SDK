//
// Created by mathias on 04.04.19.
//

#include <string>
#include "Dolos_SDK/RemoteCommand/MoveCommand.hpp"

using dolos::commands::MoveEvent;


/*****************
******************
*** MOVE EVENT ***
******************
*****************/

MoveEvent::MoveEvent(long x, long y) {
    _coordX = x;
    _coordY = y;
}

long MoveEvent::getCoordY() {
    return _coordY;
}

long MoveEvent::getCoordX() {
    return _coordX;
}
