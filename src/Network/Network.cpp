//
// Created by mathias on 01.12.18.
//

#include <Dolos_SDK/Network/Network.hpp>

#include "Dolos_SDK/Network/INetworkClient.hpp"

namespace dolos
{
    namespace network
    {
        ClientConnectedEvent::ClientConnectedEvent(std::shared_ptr<INetworkClient> &client)
            : _client(client)
        {}

        std::shared_ptr<INetworkClient> &ClientConnectedEvent::getClient() {
            return _client;
        }

        ClientDisconnectedEvent::ClientDisconnectedEvent(std::shared_ptr<INetworkClient> &client)
            : _client(client)
        {}

        std::shared_ptr<INetworkClient> &ClientDisconnectedEvent::getClient() {
            return _client;
        }

      PacketReceivedEvent::PacketReceivedEvent(std::shared_ptr<IPacket> packet, std::shared_ptr<INetworkClient> sender) {
            _packet = packet;
	    _sender = sender;
        }

        std::shared_ptr<IPacket> PacketReceivedEvent::getPacket() {
            return _packet;
        }

      std::shared_ptr<INetworkClient> PacketReceivedEvent::getSender() {
	return (_sender);
      }
    }
}
