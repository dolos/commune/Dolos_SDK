/*
** EPITECH PROJECT, 2018
** Core
** File  description:
** 
*/

#include <Dolos_SDK/ThreadPool/IThreadPool.hpp>
#include <Dolos_SDK/ThreadPool/AsyncTask.hpp>
#include <Dolos_SDK/ICore.hpp>

void dolos::AsyncTask::publishUpdate()
{
	auto &threadPool = dolos::getCore().getThreadPool();
	IThreadpool::Runnable runnable = [this]() { onUpdate(); };
	threadPool.executeOnMainThread(runnable);
}

void dolos::AsyncTask::_onPostExecute()
{
	if (!this->isCancelled())
		onPostExecute();
}

void dolos::AsyncTask::execute()
{
	auto &threadPool = dolos::getCore().getThreadPool();
	IThreadpool::Runnable runnable = [this](){ doInBackground(); };
	IThreadpool::Runnable onTaskExecuted = [this](){ _onPostExecute(); };
	threadPool.push(runnable, onTaskExecuted);
}

void dolos::AsyncTask::executeOnDedicated() {
    auto &threadPool = dolos::getCore().getThreadPool();
    IThreadpool::Runnable runnable = [this]() { doInBackground(); };
    IThreadpool::Runnable onTaskExecuted = [this](){_onPostExecute(); };
    threadPool.pushDedicated(runnable, onTaskExecuted);
}

void dolos::AsyncTask::cancel()
{
	auto &threadPool = dolos::getCore().getThreadPool();
	IThreadpool::Runnable runnable = [this]() {
		_cancelled = true;
		onCanceled();
	};
	threadPool.executeOnMainThread(runnable);
}
const std::atomic_bool &dolos::AsyncTask::isCancelled() const
{
	return _cancelled;
}
