/*
** EPITECH PROJECT, 2018
** Core
** File  description:
** Core function accessible by modules are set here
*/

#include "Dolos_SDK/ICore.hpp"

static dolos::ICore *_core = nullptr;

dolos::ICore &dolos::getCore()
{
    if (_core == nullptr)
        throw std::logic_error("ICore::getCore : Core is not initialized");
	return (*_core);
}

void dolos::setCore(dolos::ICore *core)
{
	_core = core;
}

dolos::EventManager &dolos::ICore::getEventManager() {
    return (_eventManager);
}
