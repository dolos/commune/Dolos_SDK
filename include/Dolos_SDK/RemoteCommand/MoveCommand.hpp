//
// Created by mathias on 04.04.19.
//

#ifndef DOLOS_SDK_MOVECOMMAND_HPP
#define DOLOS_SDK_MOVECOMMAND_HPP

#define DOLOS_MOVE_CMD_PACKET_NAME "Dolos_MoveCmd_Packet"
#define DOLOS_MOVE_CMD_EVENT_NAME "Dolos_MoveCmd_Event"

#include "Dolos_SDK/Network/Network.hpp"

namespace dolos
{
    namespace commands
    {
        class MoveEvent
        {
        public:
            MoveEvent() = default;
            MoveEvent(long x, long y);
            MoveEvent(const MoveEvent&) = default;
            MoveEvent &operator=(const MoveEvent&) = default;
            ~MoveEvent() = default;

        public:
            long getCoordX();
            long getCoordY();

        private:
            long _coordX= 0;
            long _coordY = 0;
        };
    }
}

#endif //DOLOS_SDK_MOVECOMMAND_HPP
