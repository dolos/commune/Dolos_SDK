//
// Created by jean-david on 6/27/18.
//

#ifndef _ILOGGER_HPP
#define _ILOGGER_HPP
#include <string>

namespace dolos
{
    enum TYPE_LOGGING {
	    STDOUT,
	    FILE
    };
	enum TYPE_LEVEL {
		ERROR,
		DEBUG,
		INFO
	};
    class Ilogger {
    	public:
    		virtual void Log(const std::string &message,TYPE_LEVEL level = dolos::TYPE_LEVEL::INFO,TYPE_LOGGING out = dolos::TYPE_LOGGING::STDOUT) = 0;
    };
}
#endif //_ILOGGER_HPP
