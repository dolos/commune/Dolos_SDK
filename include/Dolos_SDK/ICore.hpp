/*
** EPITECH PROJECT, 2018
** Core
** File  description:
** 
*/

#ifndef CORE_ICORE_HPP
#define CORE_ICORE_HPP

#include "ModuleLoader/IModuleLoader.hpp"
#include "ModuleLoader/ISystemLoader.hpp"
#include "Logger/Ilogger.hpp"
#include "IRessourcesManager.hpp"
#include "ThreadPool/IThreadPool.hpp"
#include "EventManager/EventManager.hpp"
#include "Network/INetworkManager.hpp"
#include <functional>
#include <memory>

namespace dolos
{

  class Drone;

  /**
   * This class provide access to the core functionality while protecting
   * system functionality
   */
	class ICore
	{
	public:
		ICore() = default;
		ICore(const ICore&) = delete;
		ICore &operator=(const ICore&) = delete;
		virtual ~ICore() = default;

	public:
		virtual IModuleLoader &getModuleLoader() = 0;
		virtual IRessourcesManager &getRessourcesManager() = 0;
		virtual IThreadpool &getThreadPool() = 0;
		virtual Ilogger &getLogger() = 0;
		virtual network::INetworkManager &getNetworkManager() = 0;
		virtual dolos::Drone &getDrone() = 0;
        EventManager &getEventManager();

	private:
	    EventManager _eventManager;
	};

	/**
	 * Access the core instance
	 * @return A point to the core instance
	 */
	ICore &getCore();

	/**
	 * Set the core instance
	 * Can only be used once
	 * @param core
	 */
	void setCore(ICore *core);
}

#endif //CORE_ICORE_HPP
