//
// Created by mathias on 01.12.18.
//

#ifndef CORE_INETWORKPROVIDER_HPP
#define CORE_INETWORKPROVIDER_HPP

#include <Dolos_SDK/Network/Network.hpp>

namespace dolos
{
    namespace network
    {
        class IPacketHandler
        {
        public:
            IPacketHandler() = default;
            IPacketHandler(const IPacketHandler&) = default;
            IPacketHandler(IPacketHandler&&) = delete;
            IPacketHandler &operator=(const IPacketHandler&) = default;
            virtual ~IPacketHandler() = default;

        public:
	  virtual void onPacketReceived(std::shared_ptr<IPacket> packet, std::shared_ptr<INetworkClient> client) = 0;
            virtual std::shared_ptr<IPacket> instantiate(const std::string &name, const std::string &rawData) = 0;
        };

        class INetworkProvider
        {
        public:
            INetworkProvider() = default;
            INetworkProvider(const INetworkProvider&) = delete;
            INetworkProvider(INetworkProvider&&) = delete;
            INetworkProvider &operator=(const INetworkProvider&) = delete;
            virtual ~INetworkProvider() = default;

        public:
            virtual void setSerializer(ISerializer *serializer) = 0;
            virtual void setConnectionHandler(IConnectionHandler *handler) = 0;
            virtual void setPacketHandler(IPacketHandler *handler) = 0;
            /**
             * Open server
             * @param port Port to open
             * @param proto Protocol to use
             * @return True if the server is open on the defined port using the defined Protocol
             */
            virtual bool open(unsigned int port, Protocol proto) = 0;
            /**
             * Close server, does not close the related clients
             * @param port Port to close
             */
            virtual void close(unsigned int port) = 0;
        };
    }
}

#endif //CORE_INETWORKPROVIDER_HPP
