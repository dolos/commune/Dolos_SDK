//
// Created by mathias on 06.11.18.
//

#ifndef CORE_NETWORK_HPP
#define CORE_NETWORK_HPP

#include <memory>
#include <string>

# define DOLOS_EVENT_CONNECTION "DOLOS_CLIENT_CONNECTED_EVENT"
# define DOLOS_EVENT_DISCONNECTION "DOLOS_CLIENT_DISCONNECTED_EVENT"
# define DOLOS_EVENT_PACKET_RECEIVED "DOLOS_PACKET_RECEIVED_EVENT"

namespace dolos
{
    namespace network {

        enum Protocol {
            TCP = 0,
            UDP
        };

        enum FilterPriority
        {
            LOWEST = 0,
            LOW,
            NORMAL,
            HIGH,
            HIGHEST,
            MONITOR
        };

        /**
         * Interface allowing serialization of packets
         */
        class IPacket {
        public:
            IPacket() = default;
            IPacket(const IPacket &) = default;
            IPacket(IPacket &&) = default;
            IPacket &operator=(const IPacket &) = default;
            virtual ~IPacket() = default;

        public:
            virtual std::string getName() const = 0;
            virtual std::string serialize() const = 0;
        };

        /**
         * Packet filter, used by modules to perform network modifications
         * Also used by the server to advise the NetworkManager when packets are sent / received
         */
        class ISerializer
        {
        public:
            ISerializer() = default;
            ISerializer(const ISerializer&) = default;
            ISerializer(ISerializer&&) = default;
            ISerializer &operator=(const ISerializer&) = default;
            virtual ~ISerializer() = default;

        public:
            virtual void onPacketSending(IPacket &packet) = 0;
            virtual void onPacketReceiving(IPacket &packet) = 0;
            virtual void onDataSending(std::string &rawData) = 0;
            virtual void onDataReceived(std::string &rawData) = 0;
        };

        class INetworkClient;
        /**
         * Callback class used by the server to advise the INetworkManager that a client connect / disconnect
         * NetworkManager shall trigger the ClientConnected / ClientDisconnected event
         */
        class IConnectionHandler
        {
        public:
            IConnectionHandler() = default;
            IConnectionHandler(const IConnectionHandler&) = default;
            IConnectionHandler(IConnectionHandler&&) = default;
            IConnectionHandler &operator=(const IConnectionHandler&) = default;
            virtual ~IConnectionHandler() = default;

        public:
            virtual void onConnection(std::shared_ptr<INetworkClient> client) = 0;
            virtual void onDisconnection(std::shared_ptr<INetworkClient> client) = 0;
        };

        /**
         * Event triggered when a client is connected
         */
        class ClientConnectedEvent
        {
        public:
            ClientConnectedEvent() = default;
            ClientConnectedEvent(std::shared_ptr<INetworkClient> &client);
            ClientConnectedEvent(const ClientConnectedEvent&) = default;
            ClientConnectedEvent(ClientConnectedEvent&&) = delete;
            ClientConnectedEvent &operator=(const ClientConnectedEvent&) = default;
            ~ClientConnectedEvent() = default;

        public:
            std::shared_ptr<INetworkClient> &getClient();

        private:
            std::shared_ptr<INetworkClient> _client;
        };

        /**
         * Event triggered when a client is disconnected
         */
        class ClientDisconnectedEvent
        {
        public:
            ClientDisconnectedEvent() = default;
            ClientDisconnectedEvent(std::shared_ptr<INetworkClient> &client);
            ClientDisconnectedEvent(const ClientDisconnectedEvent&) = default;
            ClientDisconnectedEvent(ClientDisconnectedEvent&&) = delete;
            ClientDisconnectedEvent &operator=(const ClientDisconnectedEvent&) = default;
            ~ClientDisconnectedEvent() = default;

        public:
            std::shared_ptr<INetworkClient> &getClient();

        private:
            std::shared_ptr<INetworkClient> _client;
        };

        /**
         * Event triggered when a packet is received
         */
        class PacketReceivedEvent
        {
        public:
            PacketReceivedEvent() = default;
	  PacketReceivedEvent(std::shared_ptr<IPacket> packet, std::shared_ptr<INetworkClient> sender);
            PacketReceivedEvent(const PacketReceivedEvent&) = default;
            PacketReceivedEvent(PacketReceivedEvent&&) = delete;
            PacketReceivedEvent &operator=(const PacketReceivedEvent&) = default;
            ~PacketReceivedEvent() = default;

        public:
            std::shared_ptr<IPacket> getPacket();
	  std::shared_ptr<INetworkClient> getSender();
        private:
            std::shared_ptr<IPacket> _packet;
	  std::shared_ptr<INetworkClient> _sender;

        };
    }
}

#endif //CORE_NETWORK_HPP
