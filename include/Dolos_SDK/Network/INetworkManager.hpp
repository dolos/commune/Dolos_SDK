//
// Created by mathias on 09.11.18.
//

#ifndef CORE_INETWORKMANAGEER_HPP
#define CORE_INETWORKMANAGEER_HPP

#include <functional>
#include <Dolos_SDK/Network/Network.hpp>

namespace dolos
{
    namespace network
    {
        using PacketInstantiater = std::function<std::shared_ptr<IPacket>(const std::string &rawData)>;

        class INetworkManager
        {
        public:
            INetworkManager() = default;
            INetworkManager(const INetworkManager&) = delete;
            INetworkManager(INetworkManager&&) = delete;
            INetworkManager &operator=(const INetworkManager&) = delete;
            virtual ~INetworkManager() = default;
        public:
            // These function are used to register / remove global packets filters
            virtual void addFilter(std::shared_ptr<ISerializer> filter, FilterPriority priority) = 0;
            virtual void removeFilter(std::shared_ptr<ISerializer> filter, FilterPriority priority) = 0;

            /*
             *  These functions are used for deserialization purpose
             *  Devs must register a packet instantiater in order to be able to create it from the serialized string
             */
            virtual void registerInstantiater(const std::string &packetName, PacketInstantiater instantiater) = 0;
        };
    }
}

#endif //CORE_INETWORKMANAGEER_HPP
