//
// Created by mathias on 29.11.18.
//

#ifndef CORE_INETWORKCLIENT_HPP
#define CORE_INETWORKCLIENT_HPP

#include <Dolos_SDK/Network/Network.hpp>

namespace dolos
{
    namespace network
    {
        class INetworkClient
        {
        public:
            INetworkClient() = default;
            INetworkClient(const INetworkClient&) = delete;
            INetworkClient(INetworkClient&&) = delete;
            INetworkClient &operator=(const INetworkClient&) = delete;
            virtual ~INetworkClient() = default;

        public:
            virtual void send(IPacket &packet) = 0;
            virtual void close() = 0;
            virtual bool isOpen() = 0;
            virtual Protocol getProtocol() = 0;
        };
    }
}

#endif //CORE_INETWORKCLIENT_HPP
