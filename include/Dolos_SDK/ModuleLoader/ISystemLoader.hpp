/*
** EPITECH PROJECT, 2018
** Core
** File  description:
** 
*/

#ifndef CORE_ISYSTEMLOADER_HPP
#define CORE_ISYSTEMLOADER_HPP

#include "Dolos_SDK/IRessourcesManager.hpp"
#include <Dolos_SDK/Network/INetworkProvider.hpp>

namespace dolos
{
    class ISystemLoader {
	public:
		ISystemLoader() = default;
		ISystemLoader(const ISystemLoader&) = default;
		ISystemLoader &operator=(const ISystemLoader&) = default;
		virtual ~ISystemLoader() = default;
	public:
		virtual void setRessourcesManager(IRessourcesManager *module) = 0;
		virtual void setNetworkServer(network::INetworkProvider *networkServer) = 0;
	};
}

#endif //CORE_SYSTEMLOADER_HPP