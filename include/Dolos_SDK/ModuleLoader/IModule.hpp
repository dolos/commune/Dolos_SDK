/*
** EPITECH PROJECT, 2018
** Dolos_SDK
** File  description:
** 
*/

#ifndef DOLOS_SDK_IMODULE_HPP
#define DOLOS_SDK_IMODULE_HPP

#include <functional>
#include <string>

# define __exported extern "C"

// Symbols used to load modules
# define FNC_PRELOAD_STR "fncPreload"
# define FNC_PRELOAD(core) void fncPreload(dolos::ICore *(core))

# define FNC_LOAD_STR "fncLoad"
# define FNC_LOAD(core) dolos::IModule *fncLoad(dolos::ICore *(core))

# define FNC_SYS_LOAD_STR "fncLoad"
# define FNC_SYS_LOAD(core, setter) dolos::IModule *fncLoad(dolos::ICore *(core), dolos::ISystemLoader *(setter))

# define FNC_UNLOAD_STR "fncUnload"
# define FNC_UNLOAD void fncUnload()

# define FNC_MODULE_DEPENDENCY_STR "fncModuleDependency"
# define FNC_MODULE_DEPENDENCY const char **fncModuleDependency()

# define FNC_MODULE_DEPENDENCY_SIZE_STR "fncModuleDependencySize"
# define FNC_MODULE_DEPENDENCY_SIZE int fncModuleDependencySize()

# define FNC_MODULE_NAME_STR "fncModuleName"
# define FNC_MODULE_NAME const char *fncModuleName()

# define FNC_MODULE_TYPE_STR "fncModuleType"
# define FNC_MODULE_TYPE int fncModuleType()
# define MODULE_OTHER 1
# define MODULE_SYSTEM 2

namespace dolos
{
	class IModule
	{
	public:
		IModule() = default;
		IModule(const IModule&) = delete;
		IModule &operator=(const IModule&) = delete;
		virtual ~IModule() = default;
		inline bool operator==(const IModule &module) const
		{
			return getName() == module.getName();
		}

	public:
		virtual std::string getName() const = 0;
		virtual void onEachFrame() noexcept = 0;
	};
}
#endif //DOLOS_SDK_IMODULE_HPP