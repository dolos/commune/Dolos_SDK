/*
** EPITECH PROJECT, 2018
** Dolos_SDK
** File  description:
** 
*/

#ifndef DOLOS_SDK_IMODULELOADER_HPP
#define DOLOS_SDK_IMODULELOADER_HPP

#include "Dolos_SDK/ModuleLoader/IModule.hpp"

namespace dolos
{
	class IModuleLoader
	{
	public:
		IModuleLoader() = default;
		IModuleLoader(const IModuleLoader&) = delete;
		IModuleLoader &operator=(const IModuleLoader&) = delete;
		virtual ~IModuleLoader() = default;

	public:
		virtual IModule *getModule(const char *name) noexcept = 0;
	};
}

#endif //DOLOS_SDK_IMODULELOADER_HPP
