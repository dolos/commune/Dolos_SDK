/*
** EPITECH PROJECT, 2018
** Core
** File  description:
** 
*/

#ifndef CORE_ASYNCTASK_BASE_HPP
#define CORE_ASYNCTASK_BASE_HPP

#include <atomic>

namespace dolos
{
	/**
	 * Base class for Asynchronous tasks
	 */
	class AsyncTask
	{
	public:
		AsyncTask() = default;
		AsyncTask(const AsyncTask&) = delete;
		AsyncTask &operator=(const AsyncTask&) = delete;
		virtual ~AsyncTask() = default;
	protected:
		/**
		 * Executed in the background
		 * This should never be an infinite loop
		 */
		virtual void doInBackground() = 0;
		/**
		 * Executed on main thread, not called if task was cancelled
		 */
		virtual void onPostExecute() = 0;
		/**
		 * Executed when an update is published
		 * Executed on main thread
		 */
		virtual void onUpdate() = 0;
		/**
		 * Called instead of postExecute when task is canceled, running on
		 * main thread
		 */
		virtual void onCanceled() = 0;
		/**
		 * Call the OnUpdate function on the main thread
		 */
		void publishUpdate();

	protected:
		void _onPostExecute();

	public:
		/**
		 * Start the task execution
		 */
		void execute();
		/**
		 * Execute on dedicated thread
		 */
		void executeOnDedicated();
		/**
		 * Tell the task to cancel
		 */
		void cancel();

	public:
		const std::atomic_bool &isCancelled() const;

	private:
		std::atomic_bool _cancelled = false;
	};
}

#endif //CORE_ASYNCTASK_BASE_HPP
