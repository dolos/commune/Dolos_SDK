/*
** EPITECH PROJECT, 2018
** Core
** File  description:
** 
*/

#ifndef CORE_THREADPOOL_HPP
#define CORE_THREADPOOL_HPP

#include <functional>

namespace dolos
{
	class IThreadpool
	{
	public:
		using Runnable = std::function<void()>;
	public:
		IThreadpool() = default;
		IThreadpool(const IThreadpool&) = delete;
		IThreadpool &operator=(const IThreadpool&) = delete;
		virtual ~IThreadpool() = default;

	public:
		/**
		 * Push a task to run asynchronously
		 * @param runnable Run async func
		 * @param onOperationComplete Run on main thread
		 */
		virtual void push(const Runnable &runnable, const Runnable &onOperationComplete)= 0;
		/**
		 * Execute runnable on main thread
		 * @param runnable Func to execute
		 */
		virtual void executeOnMainThread(const Runnable &runnable) = 0;
		/**
		 * Execute on dedicated thread
		 * @param runnable
		 * @param onOperationComplete
		 */
		virtual void pushDedicated(const Runnable &runnable, const Runnable &onOperationComplete) = 0;

	};
}

#endif //CORE_THREADPOOL_HPP
