/*
** EPITECH PROJECT, 2018
** Dolos_SDK
** File  description:
** Ressource Manager for handling access to files
*/

#ifndef DOLOS_SDK_IRESSOURCESMANAGER_HPP
#define DOLOS_SDK_IRESSOURCESMANAGER_HPP

#include <fstream>
#include <Dolos_SDK/ModuleLoader/IModule.hpp>

namespace dolos
{
	class IRessourcesManager
	{
	public:
		enum AccessResult
		{
			NOT_OK = 0,
			OK
		};

		using OnReadAccess = std::function<void(std::ifstream &stream)>;
		using OnWriteAccess = std::function<void(std::ofstream &stream)>;
		using OnAsyncReadAccess = std::function<void(std::ifstream &stream,
			AccessResult status)>;
		using OnAsyncWriteAccess = std::function<void(std::ofstream &stream,
			AccessResult status)>;

	public:
		IRessourcesManager() = default;
		IRessourcesManager(const IRessourcesManager&) = delete;
		IRessourcesManager(const IRessourcesManager&&) = delete;
		IRessourcesManager &operator=(const IRessourcesManager&) = delete;
		IRessourcesManager &operator=(const IRessourcesManager&&) = delete;
		virtual ~IRessourcesManager() = default;
	public:
		/**
		 * Synchronous read access on file
		 * @param path Path to file
		 * @param onReadAccess Action to perform on the input stream
		 * @param mode Open mode
		 */
		virtual AccessResult accessRead(const std::string &path,
		                                const OnReadAccess &onReadAccess,
		                                std::ios_base::openmode mode = std::ios_base::in) = 0;
		/**
		 * Synchronous write access to file
		 * @param path Path to file
		 * @param onWriteAccess Action to perform on the output stream
		 * @param mode Open mode
		 */
		virtual AccessResult accessWrite(const std::string &path,
		                                 const OnWriteAccess &onWriteAccess,
		                                 std::ios_base::openmode mode = std::ios_base::out) = 0;

		/**
		 * Asynchronous read access to file
		 * @param path Path to file
		 * @param onReadAccess Action to perform on the input stream
		 * @param mode Open mode
		 */
		virtual void asyncAccessRead(const std::string &path,
		                             const OnAsyncReadAccess &onReadAccess,
		                             std::ios_base::openmode mode = std::ios_base::in) = 0;

		/**
		 * Asynchronous write access to file
		 * @param path Path to file
		 * @param onWriteAccess Actio to perform on the output stream
		 * @param mode Open mode
		 */
		virtual void asyncAccessWrite(const std::string &path,
		                                 const OnAsyncWriteAccess &onWriteAccess,
		                                 std::ios_base::openmode mode = std::ios_base::out) = 0;
	};
}

#endif //DOLOS_SDK_IRESSOURCESMANAGER_HPP