//
// Created by mathias on 06.11.18.
//

#ifndef CORE_EVENTMANAGER_HPP
#define CORE_EVENTMANAGER_HPP

/*
** EPITECH PROJECT, 2018
** Core
** File description:
** EventManagerNew
*/

#ifndef EVENTMANAGERNEW_HPP_

#define EVENTMANAGERNEW_HPP_
#include <functional>
#include <string>
#include <list>
#include <memory>
#include <unordered_map>
#include <algorithm>
#include <typeinfo>
#include <typeindex>

namespace dolos
{
    /**
     * Abstract class used for subscriber listing
     */
    class ASubscriber
    {
    public:
        ASubscriber() = default;
        ASubscriber(const ASubscriber&) = default;
        ASubscriber &operator=(const ASubscriber&)= default;
        virtual ~ASubscriber() = default;
        bool operator==(const std::string &eventName) const { return (_eventName == eventName); }
    protected:
        ASubscriber(const std::string &eventName) : _eventName(eventName) {};

    private:
        std::string _eventName;
    };

    /**
     * Specialized class used for subscriber callback
     * @tparam EventClass
     */
    template <class EventClass>
    class Subscriber : public ASubscriber
    {
    public:
        Subscriber() = default;
        Subscriber(const Subscriber&) = default;
        Subscriber &operator=(const Subscriber&) = default;
        Subscriber(const std::string &name, const std::function<void
                (EventClass&)> &callback)
                : dolos::ASubscriber(name), _callback(callback) {}
        ~Subscriber() override = default;
    public:
        /**
         * Call the callback with the templated event
         * @param event
         */
        void operator()(EventClass &event) const
        {
            _callback(event);
        }
    private:
        //Store the callback
        std::function<void(EventClass&)> _callback;
    };

    class EventManager
    {
    public:
        EventManager() {};
        ~EventManager() = default;
        /**
         * Add a subscriber to an event
         * @tparam EventClass
         * @param eventName The event name
         * @param callback The callback function (void(EventClass&))
         */
        template <class EventClass>
        int  subscribe(const std::string &eventName,
                       const std::function<void(EventClass&)> &callback)
        {
            static int id = 0;
            std::unique_ptr<Subscriber<EventClass>> sub =
                    std::make_unique<Subscriber<EventClass>>(eventName, callback);
            _subscribers.emplace(std::make_pair(std::type_index(typeid(EventClass)),std::move(sub)));
            id++;
            return id;
        }

        /**
        * Delete a subscriber to an event
        * @tparam EventClass
        * @param eventName The event name
        * @param callback The callback function (void(EventClass&))
        */
        template <class EventClass>
        void unsubscribe(const std::string &eventName,
                         int id)
        {
            const std::type_index& classtype(typeid(EventClass));
            int i = 0;

            for (auto it =_subscribers.begin(); it != _subscribers.end(); i++, ++it) {
                // EventHandler of right type
                if (it->first == classtype && *(it->second) == eventName && id == i) {
                    it = _subscribers.erase(it);
                    return;
                }
            }

        }

        /**
         * Post the event to all its subscribers
         * @tparam EventClass
         * @param eventName The event name
         * @param event The event itself
         */
        template <class EventClass>
        void post(const std::string &eventName, EventClass &event)
        {
            const std::type_index& classtype(typeid(EventClass));

            for ( const auto &it : _subscribers ) {
                // EventHandler of right type
                if (*(it.second) == eventName) {
                    Subscriber<EventClass> &full = reinterpret_cast<Subscriber<EventClass>&>(*(it.second));
                    full(event);
                }
            }
        }
    private:
        // Store a list of ptr of subscribers
        std::unordered_multimap<std::type_index ,std::unique_ptr<ASubscriber>> _subscribers;
    };

}
#endif /* !EVENTMANAGERNEW_HPP_ */


#endif //CORE_EVENTMANAGER_HPP
